package testgraf;

import javax.swing.JButton;
import javax.swing.SwingUtilities;

import javafx.application.Application;
import javafx.embed.swing.SwingNode;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class tt extends Application{
	@SuppressWarnings("rawtypes")
	@Override
	public void start(Stage stage) throws Exception {
		 final SwingNode swingNode = new SwingNode();

	        createSwingContent(swingNode);

	        StackPane pane = new StackPane();
	        pane.getChildren().add(swingNode);

	        stage.setTitle("Swing in JavaFX");
	        stage.setScene(new Scene(pane, 250, 150));
	        stage.show();
	        }

	    private void createSwingContent(final SwingNode swingNode) {
	    	
	        SwingUtilities.invokeLater(new Runnable() {
	            @Override
	            public void run() {
	                swingNode.setContent(new JButton("Click me!"));
	            }
	        });
	    }
	public static void main(String[] args) {
		// TODO Auto-generated method stub
launch(args);
	}



}
