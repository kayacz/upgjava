

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * Zobrazeni mapy
 * 
 * @author
 */
public class GamePanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public Terraine terraine;
	public NamedPosition shooter, target;
	public NamedPosition hitSpot;
	public Wind wind;
	public Trajectory trajectory;
	public double wid;//��rka okna
	public double h;//v��ka okna

	public double getWid() {
		return wid;
	}

	public double getH() {
		return h;
	}

	public void setWid(double wid) {
		this.wid = wid;
	}

	public void setH(double h) {
		this.h = h;
	}

	public GamePanel() {
		super();

	}
   
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.translate(10, 10);

		double scale = getScale();
		if (terraine != null) {
			terraine.draw(g2, scale);
		}

		if (trajectory != null) {
			trajectory.draw(g2, scale);
		}

		if (shooter != null) {
			try {
				shooter.draw(g2, scale);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (target != null) {

			try {
				target.draw(g2, scale);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (hitSpot != null) {
			hitSpot.drawHit(g2, scale);

		}
		if (wind != null) {

			g2.translate(terraine.getWidthInM() * scale + 50, 0);
			try {

				wind.draw(g2, scale);
				change();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		// TODO Auto-generated method stub

	}
   //timer-animace vetrn�ho ruk�vu
	public void change() {
		Timer windTimer = new Timer();

		windTimer.schedule(new TimerTask() {

			public void run() {
				wind.generateParams();
				wind.automaticParametrs();
				Game.gamePanel.repaint();
			}
		}, 5000,5000);
	}

	/**
	 * Vrati meritko mapy
	 * 
	 * @return meritko
	 */
	public double getScale() {
		double width = ((double) this.getWid()) / (terraine.getWidthInM());
		double height = ((double) this.getH()) / (terraine.getHeightInM());
		if (width < height) {
			return width;
		} else {
			return height;
		}
	}

}