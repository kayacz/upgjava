

import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class Trajectory {
	private ArrayList<VektorXYZ> vektor;
	public int size;

	public Trajectory(int size) {
		super();
		this.size = size;
		vektor = new ArrayList<VektorXYZ>();

	}

	public void add(double x, double y, double z) {
		vektor.add(new VektorXYZ(x, y, z));
	}

	/**
	 * Prida bod trajektorie
	 * 
	 * @param vector
	 *            vektor
	 */
	public void add(VektorXYZ vec) {
		vektor.add(vec);
	}

	public ArrayList<VektorXYZ> getVektor() {
		return vektor;
	}

	public void setVektor(ArrayList<VektorXYZ> vektor) {
		this.vektor = vektor;
	}

	/**
	 * Vrati pocet bodu trajektorie
	 * 
	 * @return pocet bodu trajektorie
	 */
	public int size() {
		return vektor.size();
	}

	/**
	 * @param g2-grafick� kontext
	 * @param scale-m���tko
	 * vykreslen� trajektorie st�ely
	 */
	public void draw(Graphics2D g2, double scale) {

		if (!vektor.isEmpty()) {

			for (int k = 0; k < vektor.size(); k++) {
				Ellipse2D.Double hit = new Ellipse2D.Double(vektor.get(k).getX() * scale * 1000,
						vektor.get(k).getY() * scale * 1000, 10, 10);

				g2.fill(hit);
			}
			

			
		}

	}
}
