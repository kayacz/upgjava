

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

import javafx.stage.Stage;

public class Wind {
	public double velocityMax[] = { 0.2, 1.5, 3.3, 5.4, 7.9, 10.7, 13.8, 17.1, 20.7, 24.4, 28.4, 32.6 };
	public double minVelocity[] = { 0, 0.3, 1.6, 3.4, 5.5, 6, 7, 7.5, 8.5, 9, 10, 11 };
	public double minChangeWindVelocity[] = { 0.025, 0.2, 0.6, 1, 1.5, 8.2, 2.5, 3, 3.5, 3.7, 4.2, 4.8, 5 };
	public Color colorOfRadar[] = { Color.WHITE, new Color(204, 255, 255), new Color(153, 255, 255),
			new Color(0, 255, 255), new Color(0, 204, 255), new Color(0, 153, 255), new Color(0, 102, 255),
			new Color(0, 0, 255), new Color(0, 0, 204), new Color(0, 0, 153), new Color(0, 0, 102), Color.BLACK };
	private double velocity = 0;// v metrech zasekundu
	private double azimuth = 0;
	private double maxVelocity = velocityMax[2];
	public Random random = new Random();
	public BufferedImage windSock;
	public int stupen = 0;// Beaufortova stupnice
	public Color color = colorOfRadar[stupen];
	public double minChange = 0.2;
	public Scanner scan = new Scanner(System.in);
	/**
	 * Konstruktor

	 */
	public Wind() {
		super();

	}

	public double getMaxVelocity() {
		return maxVelocity;
	}

	public void setMaxVelocity(double maxVelocity) {
		this.maxVelocity = maxVelocity;
	}
   /*
    * graficke nastaveni parametru vetru
    */
	public void graficMode() {
		Stage stage = new Stage();
		stage.setTitle("Vitr");
		stage.show();
	}

	// nastaveni hrace, stupne sily vetru podle stupnice
	// nastaveni max rychlostu a rychlosti
	/**
	 * @param stupen-sila vetru
	 * 
	 */
	public void setP(int stupen) {
		if (stupen >= 0 && stupen < 12) {
			this.stupen = stupen;
			setVelocity(minVelocity[this.stupen]);
			setMaxVelocity(velocityMax[this.stupen]);
			minChange = minChangeWindVelocity[this.stupen];
			this.color = colorOfRadar[stupen];
		}
		if (stupen == 12) {
			System.out.println(" opravdu chces nastavit stupen 12?");
			String volba = scan.nextLine();
			volba = volba.toLowerCase();
			if (volba.equalsIgnoreCase("ano")) {
				this.stupen = stupen;
				setVelocity(minVelocity[this.stupen]);
				setMaxVelocity(250);
				minChange = minChangeWindVelocity[this.stupen];
				this.color = colorOfRadar[stupen];
			}
			if (volba.equalsIgnoreCase("ne")) {
				System.out.println("zadej znovu");
				setP(scan.nextInt());
			}
		}
	}

	public void setStupen(int stupen) {
		this.stupen = stupen;
	}
	/**
	 * automaticke nastaveni vetru

	 */
	public void automaticParametrs() {

		if (maxVelocity > 0) {

			if (maxVelocity < velocity) {
				if (stupen <= 12) {
					setStupen(stupen + 1);
					maxVelocity = velocityMax[stupen];
					minChange = minChangeWindVelocity[stupen];
					this.color = colorOfRadar[stupen];

				}
				if (stupen == 12) {
					this.stupen++;
					System.out.println("Pozor zacina hurikan");
					setVelocity(minVelocity[this.stupen]);
					setMaxVelocity(250);
					minChange = minChangeWindVelocity[this.stupen];
					this.color = colorOfRadar[stupen];

				}

			}
			if (velocity < minVelocity[stupen] && stupen >= 1) {
				this.stupen--;
				maxVelocity = velocityMax[stupen];
				minChange = minChangeWindVelocity[stupen];
				this.color = colorOfRadar[stupen];
			}
		}
	}

	public double getVelocity() {
		return velocity;
	}

	public void setVelocity(double velocity) {

		this.velocity = velocity;

	}

	public double getAzimuth() {
		return azimuth;
	}

	public void setAzimuth(double azimuth) {

		this.azimuth = azimuth;

	}

	@Override
	public String toString() {
		return "Wind [velocity=" + velocity + ", azimuth=" + azimuth + ", maxVelocity=" + maxVelocity + ", random="
				+ random + ", windSock=" + windSock + ", stupen=" + stupen + "]";
	}
	/**
	 * generov8n9 nahodnych hodnot
	 */
	public void generateParams() {
		double minVel = minChange;

		azimuth = azimuth + 2 * Math.PI * random.nextDouble();
		double randomVelocity = velocity * minVel - (minChange * random.nextDouble()) * 2;

		double rp = Math.abs(randomVelocity);
		this.velocity = rp;
	}

	/**
	 * @param g2-grafickz kontext
	 * @param width-39rka
	 * @param height
	 * vykresleni car
	 */
	public void makeLine(Graphics2D g2, double width, double height) {
		g2.translate(80, 60);
		for (int i = 0; i < 8; i++) {
			g2.rotate(Math.toRadians(45 * (i + 1)));
			g2.drawLine(0, 0, 50, 0);
			g2.rotate(Math.toRadians(-45 * (i + 1)));
		}

		g2.setColor(Color.BLACK);

		g2.translate(-60, -40);
		g2.drawString("N", (int) ((width / 2) + 8), -12);
		g2.drawString("S", (int) ((width / 2) + 8), 105);
		String vel = String.format("%.4f%n", velocity);
		g2.drawString("Rychlost v�tru:" + vel + "" + " m/s", (int) (width / 2) - 50, 120);
		g2.translate(-60, 50);
		g2.drawString("W", (int) ((width / 2) + 8), -8);
		g2.drawString("E", (int) ((width / 2) + 125), -8);

	}
	/**
	 * vykresleni sipky
	 */
	public void makeArrow(Graphics2D g2, double height) {
		GeneralPath roundShape = new GeneralPath();
		roundShape.moveTo(0, 0);
		roundShape.lineTo(0, -25);
		roundShape.lineTo(50, 0);
		roundShape.lineTo(0, 25);
		roundShape.lineTo(0, 5);
		roundShape.lineTo(-50, 5);
		roundShape.lineTo(-50, -5);
		roundShape.lineTo(0, -5);
		Area area = new Area(roundShape);

		g2.rotate(Math.toRadians(-this.getAzimuth()));
		g2.setColor(this.color);
		g2.draw(area);
		g2.fill(area);

	}

	public void makeImage() throws IOException {
		windSock = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = windSock.createGraphics();
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		Ellipse2D el = new Ellipse2D.Double(30, 10, 100, 100);

		g2.setColor(Color.BLACK);
		g2.draw(el);
		g2.setColor(Color.BLUE);
		double width = el.getWidth();
		double height = el.getHeight();
		makeLine(g2, width, height);
		g2.translate(width, height);
		g2.setColor(Color.red);

		g2.translate(20, -110);
		makeArrow(g2, height);
	}

	public void draw(Graphics2D g2, double scale) throws IOException {
		AffineTransform transform = g2.getTransform();
		g2.setClip(null);
		makeImage();
		AffineTransform affineTransform = AffineTransform.getScaleInstance(1, 1);
		AffineTransformOp op = new AffineTransformOp(affineTransform, AffineTransformOp.TYPE_BICUBIC);
		g2.drawImage(windSock, op, 10, 0);
		g2.setTransform(transform);
	}

	/**
	 * Vrati slozku X vektoru rychlosti vetru.
	 * 
	 * @return slozka X vektoru rychlosti vetru
	 */
	public double getVx() {

		double vx = this.velocity * Math.cos(-Math.toRadians(this.azimuth));
		return vx;
	}

	/**
	 * Vrati slozku Y vektoru rychlosti vetru.
	 * 
	 * @return slozka Y vektoru rychlosti vetru
	 */
	public double getVy() {
		double vy = this.velocity * Math.sin(-Math.toRadians(this.azimuth));
		return vy;
	}

	/**
	 * Vrati slozku Z vektoru rychlosti vetru.
	 * 
	 * @return slozka Z vektoru rychlosti vetru
	 */
	public double getVz() {
		return 0D;
	}

}
