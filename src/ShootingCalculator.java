

/**
 * Trida se stara o ve�ker� v�po�ty t�kaj�c� se st�elby,
 *  tj. ur�en� m�sta dopadu st�ely, detekci z�sahu c�le apod.
 *   obsahuje atributy shooter, target a hitSpot ;
 */

import java.awt.Color;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class ShootingCalculator {
	private NamedPosition shooter, target, hitSpot;
	public double gx, gy, gz, b, deltaT;
	public Trajectory trajectory;
	public Terraine terrain;
	public Wind wind;
	public final double PREVOD = Math.PI / 180;

	public ShootingCalculator(NamedPosition shooter, NamedPosition target, Terraine terraine, Wind wind) {
		super();
		this.shooter = shooter;
		this.target = target;
		this.terrain = terraine;
		this.wind = wind;
		this.gx = 0;
		this.gy = 0;
		this.gz = -10; // gravitacni zrychleni
		this.b = 0.05; // koef. vlivu vetru / odporu vzduchu
		this.deltaT = 0.1; // casovy krok
	}

	public NamedPosition getShooter() {
		return shooter;
	}

	public NamedPosition getTarget() {
		return target;
	}

	public NamedPosition getHitSpot() {
		return hitSpot;
	}

	public void setShooter(NamedPosition shooter) {
		this.shooter = shooter;
	}

	public void setTarget(NamedPosition target) {
		this.target = target;
	}

	public void setHitSpot(NamedPosition hitSpot) {
		this.hitSpot = hitSpot;
	}

	public Trajectory getTrajectory() {
		return trajectory;
	}

	public Terraine getTerrain() {
		return terrain;
	}

	public Wind getWind() {
		return wind;
	}

	public void setTrajectory(Trajectory trajectory) {
		this.trajectory = trajectory;
	}

	public void setTerrain(Terraine terrain) {
		this.terrain = terrain;
	}

	public void setWind(Wind wind) {
		this.wind = wind;
	}

	

	public void shoot2t(double azimuth, double elevation, double startV) {
		azimuth = -azimuth;
		wind = Game.gamePanel.wind;
		trajectory = new Trajectory(4);
		trajectory.add(shooter.getX(), shooter.getY(), terrain.getAltitudeInM(shooter.getX(), shooter.getY()));
		double x = trajectory.getVektor().get(0).getX();
		double y = trajectory.getVektor().get(0).getY();
		double z = trajectory.getVektor().get(0).getZ();
		double vx = Math.cos(azimuth * Math.PI / 180) * Math.cos(elevation * Math.PI / 180) * startV;
		double vy = Math.sin(azimuth * Math.PI / 180) * Math.cos(elevation * Math.PI / 180) * startV;
		double vz = Math.sin(elevation * Math.PI / 180) * startV;

		do {

			vx = vx + gx * deltaT + (wind.getVx() - vx) * b * deltaT;
			x = x + vx * deltaT;

			vy = vy + gy * deltaT + (wind.getVy() - vy) * b * deltaT;
			y = y + vy * deltaT;

			vz = vz + gz * deltaT + (wind.getVz() - vz) * b * deltaT;
			z = z + vz * deltaT;
			trajectory.add(new VektorXYZ(x, y, z));

		} while (isValid(x, y, z) == 1);
		if (isValid(x, y, z) == 0) {
			hitSpot = new NamedPosition(x, y, "hitSpot", 30);
			if (testTargetHit()) {
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Information Dialog");
				alert.setHeaderText(null);
				alert.setContentText("Z�sah");

				alert.showAndWait();

			} else {
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Information Dialog");
				alert.setHeaderText(null);
				alert.setContentText("Vedle vzd�lenost od c�le" + hitSpot.distance(target) + "m");
				alert.showAndWait();
				
			}
		} else {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Information Dialog");
			alert.setHeaderText(null);
			alert.setContentText("Strela je mimo mapu");

			alert.showAndWait();
		}

		Game.gamePanel.trajectory = this.trajectory;

		Game.gamePanel.hitSpot = this.hitSpot;
	}

	private int isValid(double x, double y, double z) {
		if ((x <= terrain.getWidthInM()) && (x >= 0)) {
			if ((y <= terrain.getHeightInM()) && (y >= 0)) {
				if (z >= terrain.getAltitudeInM(x, y)) {
					return 1;
				} else
					return 0;
			} else
				return -1;
		} else
			return -1;

	}

	/**
	 * atribut hitSpot se vytvo�� po zavol�n� teto metody zjisti se
	 * vzdalenost a uhel ve stupnich->prevod na radiany
	 * 
	 * @param azimut-uhel
	 *            ve stupnich
	 * @param distance-vzdalenost
	 *            strely
	 * @return vraci hitshot
	 */
	/*
	 * public void shoot(double azimut,double distance){
	 * 
	 * double x=(shooter.getX()+distance*Math.cos(Math.toRadians(azimut)));
	 * double y=(shooter.getY()-distance*Math.sin(Math.toRadians(azimut)));
	 * this.hitSpot=new NamedPosition(x,y,"hitpoint",Color.ORANGE,10);
	 * Game.gamePanel.hitSpot = this.hitSpot; }
	 */

	/**
	 * metoda testuje jestli je cil v oblasti zasahu
	 * 
	 * @return true nebo false
	 */
	public boolean testTargetHit() {
		double distance = hitSpot.distance(target);
		double hisize = hitSpot.getSize() / 2;
		if (distance <= hisize) {
			return true;
		}
		return false;
	}
}
