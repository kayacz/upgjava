

import java.awt.BorderLayout;
import java.awt.Color;

import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.JFrame;
import javax.swing.border.EmptyBorder;

import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

import javafx.scene.text.Text;

public class Game {
	private static Button home, bod, konec;
	private static Button graf1;
	private static Button graf2;
	private static MenuButton bt;
	private static MenuItem nSo, nSt, nRt;
	private static Button windPar;
	private static JFrame frame, frame2, frame3;
	private static BorderLayout br;
	private static JFXPanel char1;
	private static TextField tx, tx2, v0;
	static NamedPosition shooter;
	static NamedPosition target;
	static Terraine terraine;
	public static Wind wind;
	static ShootingCalculator sh;
	static GamePanel gamePanel = new GamePanel();
	static Trajectory trajectory;
	static TerrainFileHandler terrainFileHandler = new TerrainFileHandler();
	static String deFmAP = "rovny1metr.ter";
	static double elevaceGraf = 0;
	static double v0Graf = 0;
	static BorderPane an = new BorderPane();
	static Chart1 chart = new Chart1();
	static LineChart<Number, Number> lineChart;

	public static void initAndShowGUI() {
		// This method is invoked on the EDT thread
		frame = new JFrame("Swing and JavaFX");
		br = new BorderLayout();
		frame.setLayout(br);
		final JFXPanel fxPanel = new JFXPanel();
		final JFXPanel fxPanel2 = new JFXPanel();
		gamePanel.shooter = shooter;
		gamePanel.target = target;
		gamePanel.terraine = terraine;
		gamePanel.wind = wind;

		frame.add(gamePanel, BorderLayout.CENTER);
		frame.add(fxPanel2, BorderLayout.SOUTH);
		frame.add(fxPanel, BorderLayout.EAST);
		frame.pack();
		frame.addComponentListener(new ComponentListener() {
			public void componentResized(ComponentEvent e) {
				gamePanel.setWid(frame.getWidth());
				gamePanel.setH(frame.getHeight());

			}

			@Override
			public void componentMoved(ComponentEvent e) {
			}

			@Override
			public void componentShown(ComponentEvent e) {

			}

			public void componentHidden(ComponentEvent e) {
			}

		});

		frame.setSize(600, 500);
		frame.setVisible(true);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				initFX(fxPanel, 1);
				initFX(fxPanel2, 2);

			}
		});
	}

	public static void initObject() {
		wind = new Wind();
		terrainFileHandler.loadTerFile(deFmAP); // defaultni mapa
		terraine = new Terraine(terrainFileHandler.getTerrain(), terrainFileHandler.getDeltaX(),
				terrainFileHandler.getDeltaY());
		shooter = new NamedPosition((terrainFileHandler.getShooterX() * terrainFileHandler.getDeltaX() / 1000.0),
				(terrainFileHandler.getShooterY() * terrainFileHandler.getDeltaY() / 1000), "shooter", 4);
		target = new NamedPosition(terrainFileHandler.getTargetX() * terrainFileHandler.getDeltaX() / 1000.0,
				terrainFileHandler.getTargetY() * terrainFileHandler.getDeltaY() / 1000.0, "target", 4);
		sh = new ShootingCalculator(shooter, target, terraine, wind);
	}

	private static void initFX(JFXPanel fxPanel, int volba) {
		// This method is invoked on the JavaFX thread
		if (volba == 1) {
			Scene scene = createScene();

			fxPanel.setScene(scene);

		}
		if (volba == 2) {
			Scene scene2 = createScene2();
			fxPanel.setScene(scene2);
		}
		if (volba == 3) {
			Scene scene3 = createScene3();
			fxPanel.setScene(scene3);
		}
		if (volba == 4) {
			Scene scene4 = chart1();
			fxPanel.setScene(scene4);
		}
		if (volba == 5) {
			Scene scene5 = createScene5();
			fxPanel.setScene(scene5);
		}

	}

	private static Scene createScene2() {
		GridPane gr = new GridPane();
		Scene scene = new Scene(gr, 300, 80, null);
		gr.setVgap(10);
		gr.setHgap(10);
		Label azimuth = new Label("azimut");
		tx = new TextField();
		gr.add(azimuth, 1, 1);
		gr.add(tx, 1, 2);
		Label elevace = new Label("elevace");
		tx2 = new TextField();
		gr.add(elevace, 2, 1);
		gr.add(tx2, 2, 2);
		Label v0L = new Label("po��te�n� rychlost");
		v0 = new TextField();
		gr.add(v0L, 3, 1);
		gr.add(v0, 3, 2);
		Button pal = new Button("Pal");
		pal.setOnAction(event -> shoot());
		gr.add(pal, 4, 2);
		return (scene);
	}

	public static void shoot() {
		if (!(tx == null && tx2 == null && v0 == null)) {
			double elevace = Double.parseDouble(tx2.getText());
			double azimut = Double.parseDouble(tx.getText());

			double startV = Double.parseDouble(v0.getText());
			sh.shoot2t(azimut, elevace, startV);
		}
	}

	public static void changeWind(ActionEvent ac) {
		if (tx != null && !tx.getText().equalsIgnoreCase(wind.getAzimuth() + "")) {
			double azimuth = Double.parseDouble(tx.getText());
			wind.setAzimuth(azimuth);
		}
		if (tx2 != null && !tx2.getText().equalsIgnoreCase(wind.stupen + "")) {
			int stupen = Integer.parseInt(tx2.getText());
			wind.setP(stupen);
		}
	}

	private static Scene createScene3() {
		GridPane gr = new GridPane();
		Scene scene = new Scene(gr, 300, 60);
		gr.setVgap(10);
		gr.setHgap(10);
		Label label = new Label("azimuth v�tru");
		tx = new TextField();
		tx.setText(wind.getAzimuth() + "");
		tx.setOnKeyPressed(event -> {
			if (event.getCode() == KeyCode.ENTER) {
				if (tx != null && !tx.getText().equalsIgnoreCase(wind.getAzimuth() + "")) {
					double azimuth = Double.parseDouble(tx.getText());
					wind.setAzimuth(azimuth);
				}
			}
		});
		Label label2 = new Label("sila vetru");
		tx2 = new TextField();
		tx2.setOnKeyPressed(event -> {
			if (event.getCode() == KeyCode.ENTER) {
				if (tx2 != null && !tx2.getText().equalsIgnoreCase(wind.stupen + "")) {
					int stupen = Integer.parseInt(tx2.getText());
					wind.setP(stupen);
				}
			}
		});
		Button button = new Button("zm�n");
		button.setOnAction(event -> changeWind(event));
		tx2.setText(wind.stupen + "");
		gr.add(label, 2, 3);
		gr.add(tx, 3, 3);
		gr.add(label2, 2, 4);
		gr.add(tx2, 3, 4);
		gr.add(button, 3, 5);
		return (scene);
	}

	private static Scene createScene5() {
		BorderPane br = new BorderPane();
		GridPane gr = new GridPane();
		Scene scene = new Scene(br, 300, 300);
		gr.setVgap(5);
		gr.setVgap(5);
		Label l = new Label("Zadej sklon roviny");
		tx = new TextField();
		Button button = new Button("vygeneruj");
		button.setOnAction(event -> randomSwTer());
		gr.add(l, 3, 2);
		gr.add(tx, 3, 3);
		gr.add(button, 3, 4);
		br.setCenter(gr);
		return scene;
	}

	public static void init() {
		gamePanel.shooter = shooter;
		gamePanel.target = target;
		gamePanel.terraine = terraine;
		gamePanel.wind = wind;
	}

	private static void showChartOne(ActionEvent event) {
		frame2 = new JFrame("dsds");
		JFXPanel j = new JFXPanel();
		frame2.add(j);
		frame2.setVisible(true);
		frame2.setSize(600, 500);
		frame2.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		initFX(j, 4);
	}

	public static void chooseFile(ActionEvent event) {
		deFmAP = terrainFileHandler.loadTerFile();
		if (!(deFmAP.equalsIgnoreCase("chyba"))) {
			initObject();
			frame.dispose();
			initAndShowGUI();
		}

	}

	private static void closeWindow(ActionEvent event) {
		frame.dispose();
	}

	private static Scene chart1() {

		Scene scene = new Scene(an, 300, 400);
		lineChart = chart.drawChart();
		an.setCenter(lineChart);
		GridPane gr = new GridPane();
		gr.setVgap(10);
		gr.setHgap(10);
		Label elevace = new Label("elevace");
		tx = new TextField();
		gr.add(elevace, 1, 1);
		gr.add(tx, 1, 2);
		Label v0L = new Label("po��te�n� rychlost");
		v0 = new TextField();
		gr.add(v0L, 2, 1);
		gr.add(v0, 2, 2);

		Button v = new Button("vykreslit");
		v.setOnAction(event -> changeGrafValue());
		gr.add(v, 3, 2);
		BorderPane.setAlignment(gr, Pos.CENTER);
		gr.setPadding(new Insets(10, 10, 10, 10));
		an.setBottom(gr);
		return scene;

	}

	public static void changeGrafValue() {
		if (!(tx == null && v0 == null)) {
			elevaceGraf = Double.parseDouble(tx.getText());
			v0Graf = Double.parseDouble(v0.getText());
			chart.setElevation(elevaceGraf);
			chart.setV0(v0Graf);

			lineChart = chart.drawChart();
			an.setCenter(lineChart);
		}
	}

	private static void showWind(ActionEvent event) {
		frame2 = new JFrame("dsds");
		JFXPanel j = new JFXPanel();
		frame2.add(j);
		frame2.setSize(300, 300);
		frame2.setVisible(true);
		frame2.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		initFX(j, 3);
	}

	private static void nGame() {
		shooter.nPos(terraine.getDeltaXInM() / 1000, terraine.getDeltaYInY() / 1000);
		target.nPos((terraine.getDeltaXInM() * terraine.getTerrain().length) / 1000,
				(terraine.getDeltaXInM() * terraine.getTerrain()[0].length) / 1000);
		frame.dispose();
		initAndShowGUI();
	}

	private static void initRandom() {
		terraine = new Terraine(terrainFileHandler.getTerrain(), terrainFileHandler.getDeltaX(),
				terrainFileHandler.getDeltaY());
		shooter = new NamedPosition((terrainFileHandler.getShooterX() * terrainFileHandler.getDeltaX() / 1000.0),
				(terrainFileHandler.getShooterY() * terrainFileHandler.getDeltaY() / 1000), "shooter", 4);
		target = new NamedPosition(terrainFileHandler.getTargetX() * terrainFileHandler.getDeltaX() / 1000.0,
				terrainFileHandler.getTargetY() * terrainFileHandler.getDeltaY() / 1000.0, "target", 4);
		sh = new ShootingCalculator(shooter, target, terraine, wind);
	}

	private static void sTeren() {
		frame2 = new JFrame("Nahodny sikmy teren");

		JFXPanel j = new JFXPanel();
		frame2.add(j);
		frame2.setSize(300, 300);
		frame2.setVisible(true);
		frame2.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		initFX(j, 5);
	}

	private static void randomSwTer() {
		if (tx.getText() != null ) {
			terrainFileHandler = new TerrainFileHandler();
			terrainFileHandler.generateData("s", Double.parseDouble(tx.getText()));
			initRandom();
			frame.dispose();
			initAndShowGUI();
		}
	}

	private static Scene createScene() {
		AnchorPane an = new AnchorPane();
		Scene scene = new Scene(an, 100, 400);

		Text text = new Text();
		home = new Button("Nova hra");
		home.setOnAction(event -> nGame());
		home.setPrefWidth(100);
		//bod = new Button("bodovani");
		windPar = new Button("parametry vetru");
		windPar.setPrefWidth(100);
		windPar.setOnAction(event -> showWind(event));
		graf1 = new Button("graf1");
		graf1.setPrefWidth(100);
		graf1.setOnAction(event -> showChartOne(event));
		graf2 = new Button("graf2");
		graf2.setPrefWidth(100);
		konec = new Button("konec");
		konec.setPrefWidth(100);
		konec.setOnAction(event -> closeWindow(event));
		nSo = new MenuItem("Mapa ze souboru");
		nSo.setOnAction(event -> chooseFile(event));
		nRt = new MenuItem("Rovny teren");
		nSt = new MenuItem("Sikmy teren");
		nSt.setOnAction(event -> sTeren());
		bt = new MenuButton("Nova mapa", null, nSo, nRt, nSt);
		VBox h = new VBox();

		h.getChildren().addAll(home, bt, windPar, graf1, graf2, konec);
		an.getChildren().add(h);
		AnchorPane.setTopAnchor(h, 50.0);

		return (scene);
	}

	public static void main(String[] args) {
		initObject();

		initAndShowGUI();

	}
}
