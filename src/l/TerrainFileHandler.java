package l;


import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import java.util.Random;

import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * funkci teto tridy: je manipulace s binarnim souborem nacteni vstupnich dat
 * terenu generovani nahodneho terenu vypis terenu ulozeni do binarniho souboru
 * vygenerovaneho terenu
 * 
 * @author Karolina Balejova
 * 
 *
 */
public class TerrainFileHandler {
	private int[][] terrain;
	private int collums;
	private int rows;
	private int deltaX;
	private int deltaY;
	private int shooterX;
	private int shooterY;
	private int targetX;
	private int targetY;
	/* instance tridy se pouziva pro generovani pseudo nahodnych cisel */
	public static Random ran = new Random();
	static final double mmToM = 1000.0;

	public TerrainFileHandler() {

	}

	/*
	 * metody k navraceni hodnoty privatni atributy
	 */
	public int[][] getTerrain() {
		return terrain;
	}

	public int getCollums() {
		return collums;
	}

	public int getRows() {
		return rows;
	}

	public int getDeltaX() {
		return deltaX;
	}

	public int getDeltaY() {
		return deltaY;
	}

	public int getShooterX() {
		return shooterX;
	}

	public int getShooterY() {
		return shooterY;
	}

	public int getTargetX() {
		return targetX;
	}

	public int getTargetY() {
		return targetY;
	}

	/*
	 * metody k nastaveni hodnoty atributum privatni atributy
	 */
	public void setTerrain(int[][] terrain) {
		this.terrain = terrain;
	}

	public void setCollums(int collums) {
		this.collums = collums;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public void setDeltaX(int deltaX) {
		this.deltaX = deltaX;
	}

	public void setDeltaY(int deltaY) {
		this.deltaY = deltaY;
	}

	public void setShooterX(int shooterX) {
		this.shooterX = shooterX;
	}

	public void setShooterY(int shooterY) {
		this.shooterY = shooterY;
	}

	public void setTargetX(int targetX) {
		this.targetX = targetX;
	}

	public void setTargetY(int targetY) {
		this.targetY = targetY;
	}

	/**
	 * Generuje plochy teren
	 * 
	 * @param value
	 *            (konstantni) nadmorska vyska v mm
	 * @return 2D pole nadmorskych vysek
	 */
	public int[][] generateFlatTeraine(int value) {
		int[][] terrain = new int[rows][collums];

		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < collums; j++) {
				terrain[i][j] = value;
			}

		}
		return terrain;
	}

	/**
	 * Generuje terenu typu "rovina naklonne ve smeru osy x"
	 * 
	 * @param inclination
	 *            sklon roviny (prirustek mezi dvema sloupci) sklon 0 = zcela
	 *            plochy teren<br>
	 *            sklon 1 = mezi dvema sloupci je prirustek 1 mm
	 * @return 2D pole nadmorskych vysek
	 */
	public int[][] generateSweepTeraine(double inclination) {
		terrain = new int[rows][collums];
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < collums; j++) {
				terrain[i][j] = (int) (j * inclination);
			}
		}

		return this.terrain;
	}
 
	/**
	 * metodata slouzi pro vygenerovani pseudonahodnych cisel-tedy generovani
	 * nahodnejo terenu nastaveni atributu
	 * 
	 * @param option-je
	 *            parametr, ktery urcuje jaky typ terenu se vygeneruje s-sikmy
	 *            teren(zavola se metoda generateSweepTeraine) r-plochy
	 *            teren(zavola se metoda generateFlatTeraine)
	 * @param val-slouzi
	 *            k nastaveni konstani nadmorske vysky u plocheho terenu, nebo
	 *            sklon roviny u naklonene roviny
	 * 
	 */
	public void generateData(String option, double val) {
		collums = ran.nextInt(6000)+1;
		rows = ran.nextInt(6000)+1;
		deltaX = ran.nextInt(10000 - 1000) + 2000;
		deltaY = ran.nextInt(10000 - 1000) + 200;
		shooterX = ran.nextInt(collums)+1;
		shooterY = ran.nextInt(rows)+1;
		targetX = ran.nextInt(collums)+1;
		targetY = ran.nextInt(rows)+1;

		if (option.equalsIgnoreCase("r")) {

			terrain = generateFlatTeraine((int) val);

		} else if (option.equalsIgnoreCase("s")) {
			terrain = generateSweepTeraine(val);
		}
	}

	/**
	 * Ulozi teren do binarni souboru
	 * 
	 * @param name-cesta
	 *            k binarnimu souboru
	 */
	public void saveTer(String name) {
		try (DataOutputStream os = new DataOutputStream(new FileOutputStream(name))) {
			// zapis dat
			os.writeInt(collums);
			os.writeInt(rows);
			os.writeInt(deltaX);
			os.writeInt(deltaY);
			os.writeInt(shooterX);
			os.writeInt(shooterY);
			os.writeInt(targetX);
			os.writeInt(targetY);
			for (int j = 0; j < rows; j++) {
				for (int i = 0; i < collums; i++) {
					os.writeInt(this.terrain[j][i]);
				}
			}
			os.close();
		} catch (IOException e) {
			System.err.println("Soubor nelze zapsat");
		}
	}
   public void loadTerFile(){
		FileChooser chooser = new FileChooser();
	    chooser.setTitle("Open File");
	    FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("bin files (*.ter)", "*.ter");
        chooser.getExtensionFilters().add(extFilter);
        Stage stage=new Stage();
        File file = chooser.showOpenDialog(stage);
        loadTerFile(file.getAbsolutePath());
   }
	/**
	 * metoda nacita z binarniho souboru udaje o terenu nastavi atributy pokud
	 * nebude soubor nalezen vyvola se vyjimka
	 * 
	 * @param fileName-cesta
	 *            k binarnimu souboru
	 */
	public void loadTerFile(String fileName) {
		try (DataInputStream inputStream = new DataInputStream(new FileInputStream(fileName));) {

			collums = inputStream.readInt();
			rows = inputStream.readInt();
			deltaX = inputStream.readInt();
			deltaY = inputStream.readInt();
			shooterX = inputStream.readInt();
			shooterY = inputStream.readInt();
			targetX = inputStream.readInt();
			targetY = inputStream.readInt();
			terrain = new int[rows][collums];
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < collums; j++) {
					terrain[i][j] = inputStream.readInt();
				}

			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * metoda na�ten� data srozumiteln� vyp�e na konzoli
	 * kontrola jestli neni cil nebo strelec mimo herni plochu
	 */
	public void vypis() {
		System.out.println(String.format("Pocet sloupcu: %d, pocet radku: %d", collums, rows));
		System.out.println(String.format("Rozestup mezi sloupci %d mm, mezi radky %d mm", deltaX, deltaY));
		System.out.println(String.format("Rozmery oblasti: sirka %d mm, vyska %d mm", collums * deltaX, rows * deltaY));

		if (shooterX < 0 || shooterX >= collums || shooterY < 0 || shooterY >= rows) {
			System.out.println("STRELEC JE MIMO MAPU !");
		} else {
			System.out.println(String.format("Poloha strelce: sloupec %d, radek %d, tj. x = %d mm, y = %d mm", shooterX,
					shooterY, shooterX * deltaX, shooterY * deltaY));
		}

		if (targetX < 0 || targetX >= collums || targetY < 0 || targetY >= rows) {
			System.out.println("CIL JE MIMO MAPU !");
		} else {

			System.out.println(String.format("Poloha cile: sloupec %d, radek %d, tj. x = %d mm, y = %d mm", targetX,
					targetY, targetX * deltaX, targetY * deltaY));
		}
	}

}
