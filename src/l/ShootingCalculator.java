package l;




/**
 * Trida se stara o ve�ker� v�po�ty t�kaj�c� se st�elby,
 *  tj. ur�en� m�sta dopadu st�ely, detekci z�sahu c�le apod.
 *   obsahuje atributy shooter, target a hitSpot ;
 */

import java.awt.Color;

public class ShootingCalculator {
	private NamedPosition shooter, target, hitSpot;
	public double gx, gy, gz, b, deltaT;
	public Trajectory trajectory ;
	public Terraine terrain;
	public Wind wind;
	public ShootingCalculator(NamedPosition shooter, NamedPosition target,Terraine terraine,Wind wind) {
		super();
		this.shooter = shooter;
		this.target = target;
	    this.terrain=terraine;
	    this.wind=wind;
		this.gx = 0;
		this.gy = 0;
		this.gz = 10; // gravitacni zrychleni
		this.b = 0.05; // koef. vlivu vetru / odporu vzduchu
		this.deltaT = 0.1; // casovy krok
	}

	public NamedPosition getShooter() {
		return shooter;
	}

	public NamedPosition getTarget() {
		return target;
	}

	public NamedPosition getHitSpot() {
		return hitSpot;
	}

	public void setShooter(NamedPosition shooter) {
		this.shooter = shooter;
	}

	public void setTarget(NamedPosition target) {
		this.target = target;
	}

	public void setHitSpot(NamedPosition hitSpot) {
		this.hitSpot = hitSpot;
	}
	
    public Trajectory getTrajectory() {
		return trajectory;
	}

	public Terraine getTerrain() {
		return terrain;
	}

	public Wind getWind() {
		return wind;
	}

	public void setTrajectory(Trajectory trajectory) {
		this.trajectory = trajectory;
	}

	public void setTerrain(Terraine terrain) {
		this.terrain = terrain;
	}

	public void setWind(Wind wind) {
		this.wind = wind;
	}


	public void shoot2(double azimuth, double elevation, double startV) {
		azimuth = (azimuth*Math.PI/180);
		elevation = (elevation*Math.PI)/180;
		this.trajectory = new Trajectory(4);
		wind=Game.gamePanel.wind;
		VektorXYZ first = new VektorXYZ(
				shooter.getX()/1000.0,
				shooter.getY()/1000.0,
				 terrain.aplitudes(shooter.getX()/1000.0, shooter.getY()/1000.0)+1);
		System.out.println(shooter.getX() );
		VektorXYZ firstS = new VektorXYZ(
				startV * Math.cos(elevation) * Math.cos(-azimuth)*deltaT,
				startV * Math.cos(elevation) * Math.sin(-azimuth)*deltaT,
				startV * Math.sin(elevation) * deltaT);
		
		trajectory.add(first);
		
		VektorXYZ lastShootVelocity = firstS;
		VektorXYZ lastPosition = first;
		VektorXYZ secondLastPosition = firstS;
		boolean valid = true;
		System.out.println(wind.getVx());
		while(terrain.aplitudes(lastPosition.getX(), lastPosition.getY()) <=lastPosition.getZ()) {
			VektorXYZ newShootVelocity = new VektorXYZ(
					lastShootVelocity.getX()+(wind.getVx()-lastShootVelocity.getX())*b*deltaT,
					lastShootVelocity.getY()+(wind.getVy()-lastShootVelocity.getY())*b*deltaT,
					lastShootVelocity.getZ()+gz*deltaT+(wind.getVz()-lastShootVelocity.getZ())*b*deltaT);
				
			VektorXYZ newPosition = new VektorXYZ(
					lastPosition.getX()+newShootVelocity.getX()*deltaT,
					lastPosition.getY()+newShootVelocity.getY()*deltaT,
					lastPosition.getZ()+newShootVelocity.getZ()*deltaT);
			
			if(!terrain.isValidLocation(newPosition.getX(), newPosition.getY())) {
				valid = false;
				System.out.println("is not valid");
				break;
			}
	
			secondLastPosition = lastPosition;
			trajectory.add(newPosition);
			lastShootVelocity = newShootVelocity;
			lastPosition = newPosition;
			if(valid == true) {
				VektorXYZ bisected = bisection(lastPosition, secondLastPosition);
				this.hitSpot = new NamedPosition(bisected.getX(), bisected.getY(), " ", Color.ORANGE,10);
			
				
			} else {		
				this.hitSpot = null;
			}
		};	
		
	
		Game.gamePanel.trajectory=this.trajectory;

		
		Game.gamePanel.hitSpot = this.hitSpot;
	}
	private VektorXYZ bisection(VektorXYZ last, VektorXYZ secondLast) {
		VektorXYZ top = secondLast;
		VektorXYZ bottom = last;
		VektorXYZ bisected = new VektorXYZ((top.getX()+bottom.getX())/(2.0), (top.getY()+bottom.getY())/(2.0), (top.getZ()+bottom.getZ())/(2.0));
		double altitude = terrain.aplitudes(bisected.getX(), bisected.getY());
		int attempt = 0;
		while(altitude != bisected.getZ()) {
			if(Math.abs((altitude-bisected.getZ())) < 0.001) break;
			if(attempt == 100) break;
			if(altitude > bisected.getZ()) {
				bottom = bisected;
				bisected = new VektorXYZ((top.getX()+bisected.getX())/(2.0), (top.getY()+bisected.getY())/(2.0), (top.getZ()+bisected.getZ())/(2.0));
			} else {
				top = bisected;
				bisected = new VektorXYZ((bisected.getX()+bottom.getX())/(2.0), (bisected.getY()+bottom.getY())/(2.0), (bisected.getZ()+bottom.getZ())/(2.0));
			}
			altitude = terrain.aplitudes(bisected.getX(), bisected.getY());
			attempt++;
		}
		return bisected;
	}
	/**
	 * atribut hitSpot se vytvo�� po zavol�n� teto metody zjisti se
	 * vzdalenost a uhel ve stupnich->prevod na radiany
	 * 
	 * @param azimut-uhel
	 *            ve stupnich
	 * @param distance-vzdalenost
	 *            strely
	 * @return vraci hitshot
	 */
	/*
	 * public void shoot(double azimut,double distance){
	 * 
	 * double x=(shooter.getX()+distance*Math.cos(Math.toRadians(azimut)));
	 * double y=(shooter.getY()-distance*Math.sin(Math.toRadians(azimut)));
	 * this.hitSpot=new NamedPosition(x,y,"hitpoint",Color.ORANGE,10);
	 * Game.gamePanel.hitSpot = this.hitSpot; }
	 */


	/**
	 * metoda testuje jestli je cil v oblasti zasahu
	 * 
	 * @return true nebo false
	 */
	public boolean testTargetHit() {
		double a = target.distance(hitSpot) / 1000.0;
		System.out.println(a);
		if (a <= 0) {
			return true;
		}
		return false;
	}
}
