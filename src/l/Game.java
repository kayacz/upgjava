package l;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import javafx.application.Application;
import javafx.embed.swing.SwingNode;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class Game extends Application {
	public static Scanner scan = new Scanner(System.in);
	/* pro prevod mm na m */
	public static final double toMetr = 1000.0;
	/*
	 * zakladni objekty trid TerrainFileHandler,NamedPosition,Terraine
	 */
	static TerrainFileHandler terrainFileHandler = new TerrainFileHandler();
	static NamedPosition shooter;
	static NamedPosition target;
	static Terraine terraine;
	static GamePanel gamePanel = new GamePanel();
	static ShootingCalculator sh;
	public static Wind wind;
	public static Game game = new Game();
	private static Trajectory trajectory;
	/*
	 * predani parametru, vytvoreni cile a strelce
	 * 
	 */

	public static void createNamedPosion() {
		shooter = new NamedPosition((terrainFileHandler.getShooterX() * terrainFileHandler.getDeltaX()),
				(terrainFileHandler.getShooterY() * terrainFileHandler.getDeltaY()), "shooter", Color.red, 4);
		target = new NamedPosition(terrainFileHandler.getTargetX() * terrainFileHandler.getDeltaX(),
				terrainFileHandler.getTargetY() * terrainFileHandler.getDeltaY(), "target", Color.blue, 4);
	    gamePanel.setWid(400);
        gamePanel.setH(250);
	}

	private static void makeWindow() {
		JFrame window = new JFrame("A16B0001P|Karolina Balejova");
		gamePanel.shooter = shooter;
		gamePanel.target = target;
		gamePanel.terraine = terraine;
		gamePanel.wind = wind;


		gamePanel.setLayout(new BorderLayout());
        
	}
	   private void createSwingContent(final SwingNode swingNode) {
	
	        SwingUtilities.invokeLater(new Runnable() {
	            @Override
	            public void run() {
	                swingNode.setContent(gamePanel);
	            }
	        });
	    }
	@Override
	public void start(Stage stage) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("zadej nazev souboru");
		// nacteni souboru
		// terrainFileHandler.loadTerFile(scan.nextLine());
		terrainFileHandler.loadTerFile("rovny1metr.ter");
		//terrainFileHandler.generateData("s", 45);
		
		createNamedPosion();
		terraine = new Terraine(terrainFileHandler.getTerrain(), terrainFileHandler.getDeltaX(),
				terrainFileHandler.getDeltaY());

		sh = new ShootingCalculator(shooter, target, terraine, wind);
		wind = new Wind();
		

	        StackPane pane = new StackPane();
	        final SwingNode swingNode = new SwingNode();
	           
	        createSwingContent(swingNode);

	    
	        pane.getChildren().add(swingNode);
	        stage.setTitle("Swing in JavaFX");
	        stage.setScene(new Scene(pane, 400, 250));
	        stage.show();
	}
	public static void main(String[] args) {
		launch(args);
	}

}
