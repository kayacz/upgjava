package l;



import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.GeneralPath;
import java.util.ArrayList;

public class Trajectory {
private ArrayList<VektorXYZ> vektor;
public int size;
public Trajectory(int size) {
	super();
	this.size=size;
	vektor = new ArrayList<VektorXYZ>();
	
}
public void add(double x, double y, double z) {
	vektor.add(new VektorXYZ(x,y,z));
}

/**
 * Prida bod trajektorie
 * @param vector vektor
 */
public void add(VektorXYZ vec) {
	vektor.add(vec);
}

public ArrayList<VektorXYZ> getVektor() {
	return vektor;
}
public void setVektor(ArrayList<VektorXYZ> vektor) {
	this.vektor = vektor;
}
/**
 * Vrati pocet bodu trajektorie
 * @return pocet bodu trajektorie
 */
public int size() {
	return vektor.size();
}
public void draw(Graphics2D g2, double scale) {
	
	if(!vektor.isEmpty()) {
		g2.setColor(Color.blue);
		double[][] points = new double[vektor.size()][2];
		 GeneralPath path = new GeneralPath();
	        path.moveTo(vektor.get(0).getX(), vektor.get(0).getY());

       
        for (int k = 1; k <vektor.size(); k++)
        	path.lineTo(vektor.get(k).getX(), vektor.get(k).getY());
      
        g2.draw(path);
      
		
	}
	
}
}
