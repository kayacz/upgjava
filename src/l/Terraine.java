package l;


/**
 * trida obsluhuje v�hradn� mapu pracuje s polem v��ek, kter� jsme na�etli ze souboru.
 * @author Karolina
 *
 */

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.util.Random;

public class Terraine {
	private int terrain[][];
	private int deltaXInM;
	private int deltaYInM;
	private BufferedImage terrainImage;

	public Terraine(int[][] terrain, int deltaXInM, int deltaYInY) {
		super();
		this.terrain = terrain;
		this.deltaXInM = deltaXInM;
		this.deltaYInM = deltaYInY;
		makeImage();
	}

	public int[][] getTerrain() {
		return terrain;
	}

	public void setTerrain(int[][] terrain) {
		this.terrain = terrain;
	}

	public boolean isValidLocation(double x, double y) {
		if (x >= 0 && x <= getWidthInM()) {
			if (y >= 0 && y <= getHeightInM()) {
				return true;
			}
		}
		return false;
	}

	public int getDeltaXInM() {
		return deltaXInM;
	}

	public void setDeltaXInM(int deltaXInM) {
		this.deltaXInM = deltaXInM;
	}

	public int getDeltaYInY() {
		return deltaYInM;
	}

	public double getWidthInM() {
		if (terrain.length > 0) {
			return (terrain[0].length) * deltaXInM;
		}
		return 0.0;
	}

	/**
	 * Vrati vysku v metrech
	 * 
	 * @return vyska v metrech
	 */
	public double getHeightInM() {
		return (terrain.length) * deltaYInM;
	}

	public void setDeltaYInY(int deltaYInY) {
		this.deltaYInM = deltaYInY;
	}

	/**
	 * vypocita souradnice objektu
	 * 
	 * @param x-souradncie
	 *            objektu na ose x
	 * @param y/souradnice
	 *            objektu na ose y
	 * @return souradnice objektu
	 */
	public double aplitudes(double x, double y) {
		double x1 = x / (deltaXInM / 1000.0);
		double y1 = y / (deltaYInM / 1000.0);
		int x2, y2, x3, y3, x4, y4;

		x2 = (int) x1;
		y2 = (int) y1;

		if (x2 == x1 && y2 == y1) {

			return terrain[y2][x2];
		}

		if (x > 1.5 && y < x) {
			x3 = (int) Math.ceil(x1);
			y3 = y2;
		} else {
			x3 = x2;
			y3 = (int) Math.ceil(y1);
		}

		x4 = (int) Math.ceil(x1);
		y4 = (int) Math.ceil(y1);

		// smerovy vektor
		double sx2 = x2 - x1;
		double sy2 = y2 - y1;
		double sx3 = x3 - x1;
		double sy3 = y3 - y1;
		double sx4 = x4 - x1;
		double sy4 = y4 - y1;

		// delka smeroveho vektoru
		double dv2 = Math.sqrt(sx2 * sx2 + sy2 * sy2);
		double dv3 = Math.sqrt(sx3 * sx3 + sy3 * sy3);
		double dv4 = Math.sqrt(sx4 * sx4 + sy4 * sy4);

		// procentualni vzdalenost k bodum
		double p1 = 100.0 / (dv2 + dv3 + dv4);
		double p2 = dv2 * p1;
		double p3 = dv3 * p1;
		double p4 = dv4 * p1;

		// System.out.println("V��ka v bod�: " + x1 + " x " + y1 + "
		// troj�heln�k: " + x2 + " x " +y2 + " " + x3 + " x " +y3 + " " + x4 + "
		// x " +y4);
		int value = 0;
		try {
			value += (terrain[y2][x2] / 100.0) * p2;
		} catch (Exception e) {
		}
		try {
			value += (terrain[y3][x3] / 100.0) * p3;
		} catch (Exception e) {
		}
		try {
			value += (terrain[y4][x4] / 100.0) * p4;
		} catch (Exception e) {
		}

		return value;
	}

	public void draw(Graphics2D g2, double scale) {
		Rectangle2D rectangle = new Rectangle2D.Double();
		rectangle.setFrame(0, 0, (int) (getWidthInM() * scale), (int) (getHeightInM() * scale));
		g2.setColor(Color.white);
		g2.fill(rectangle);
		AffineTransform affineTransform = AffineTransform.getScaleInstance(
				(getWidthInM() * scale) / ((double) terrainImage.getWidth()),
				(getHeightInM() * scale) / ((double) terrainImage.getHeight()));

		AffineTransformOp op = new AffineTransformOp(affineTransform, AffineTransformOp.TYPE_BICUBIC);

		g2.drawImage(terrainImage, op, 0, 0);

		g2.setColor(Color.BLACK);
		g2.draw(rectangle);
		g2.setClip(rectangle);

	}

	public boolean isSame() {
		int num = Integer.MIN_VALUE;
		for (int i = 0; i < terrain.length; i++) {
			for (int j = 0; j < terrain.length; j++) {
				if (num < terrain[i][j]) {
					return false;
				}
			}
		}
		return true;
	}

	public Color getColor(int value, int rozdil, int min) {
		int result = value - min;
		int color = 0;
		int re = value - min;
		double coef = 255.0 / rozdil;
		color = (int) (coef * re);
		if (color > 255) {
			color = 255;
		}
		if (color < 0) {
			color = 0;
		}
		return new Color(color, color, color);
	}

	public void makeImage() {
		Color color = new Color(128, 128, 128);
		int colum = terrain[0].length;
		int rows = terrain.length;
		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < colum; j++) {
				max = Math.max(max, terrain[i][j]);
				min = Math.min(min, terrain[i][j]);
			}
		}
		int rozdil = max - min;
		this.terrainImage = new BufferedImage(colum, rows, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = terrainImage.createGraphics();

		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < colum; j++) {
				if (rozdil == 0) {
					g2.setColor(color);
					g2.fillRect(j, i, 1, 1);
				} else {
					g2.setColor(getColor(terrain[i][j], rozdil, min));
					g2.fillRect(i, j, 1, 1);
				}
			}
		}

	}

}

