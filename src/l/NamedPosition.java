package l;


/**
 
* metoda obsahuje polohu objektu(strelce,cile,zasahu)
 */

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class NamedPosition {
	private double x;
	private double y;
	private String positionType;
	private Color color;
	private int size;
	public BufferedImage bw;

	/*
	 * konstruktor tridy
	 */
	public NamedPosition(double x, double y, String positionType, Color color, int size) {
		super();
		this.x = x;
		this.y = y;
		this.positionType = positionType;
		this.color = color;
		this.size = size;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public String getPositionType() {
		return positionType;
	}

	public void setPositionType(String positionType) {
		this.positionType = positionType;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public double distance(NamedPosition tg) {
		
		double result = (tg.x - x) * (tg.x - x) + (tg.y - y) * (tg.y - y);
		return Math.sqrt(result);

	}

	/**
	 * vyklesleni krizku zsjisti se vyska a sirka panelu, podle velikosti
	 * rozteci mezi radky a sloupci se vykresli krizek
	 * 
	 * @param g2
	 *            graficky kontex
	 * @param width-sirka
	 *            panelu
	 * @param height-vyska
	 *            panelu
	 * @param rows-pocet
	 *            radek
	 * @param coll-pocet
	 *            sloupcu
	 */
	public void draw(Graphics2D g2, double scale) {
		GeneralPath gp = new GeneralPath();
		g2.setColor(this.color);
		double width = x * scale;
		double height = y * scale;
		g2.setStroke(new BasicStroke(this.size));
		gp.moveTo(width, height);
		gp.lineTo(width + 10, height);
		gp.moveTo(width + 5, height - 5);
		gp.lineTo(width + 5, height + 5);

		g2.draw(gp);

	}

	private static BufferedImage resizeImage(BufferedImage originalImage, int type, double scale, int width,
			int height) {
		BufferedImage resizedImage = new BufferedImage(width, height, type);
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(originalImage, 0, 0, width, height, null);
		g.dispose();

		return resizedImage;
	}

	public void testdraw(Graphics2D g2, double scale) throws IOException {
		AffineTransform transform = g2.getTransform();
		double width = x * scale;
		double height = y * scale;
		BufferedImage resizeImageJpg = null;
		if (this.positionType.equalsIgnoreCase("shooter")) {
			bw = ImageIO.read(new File("vez2.png"));
			int type = bw.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : bw.getType();
			resizeImageJpg = resizeImage(bw, type, scale, 20, 25);
		}
		if (this.positionType.equalsIgnoreCase("target")) {
			bw = ImageIO.read(new File("queen.png"));
			int type = bw.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : bw.getType();
			resizeImageJpg = resizeImage(bw, type, scale, 30, 40);
		}

		g2.setClip(null);

		AffineTransform affineTransform = AffineTransform.getScaleInstance(1, 1);
		AffineTransformOp op = new AffineTransformOp(affineTransform, AffineTransformOp.TYPE_BICUBIC);

		if (this.positionType.equalsIgnoreCase("target")) {
			g2.drawImage(resizeImageJpg, op, (int) width - 10, (int) height - 20);
		} else {
			g2.drawImage(resizeImageJpg, op, (int) width - 5, (int) height - 5);
		}

	}

	/**
	 * vyklesleni oblasti zasahu
	 * 
	 * @param g2
	 *            graficky kontex
	 * @param width-sirka
	 *            panelu
	 * @param height-vyska
	 *            panelu
	 * @param rows-pocet
	 *            radek
	 * @param coll-pocet
	 *            sloupcu
	 */
	public void drawHit(Graphics2D g2, double scale) {
		// TODO Auto-generated method stub
		g2.setColor(Color.orange);
		double width = (x) * scale;
		double height =( y) * scale;

		g2.fillOval((int)width, (int)height, 40, 40);

	}
}
