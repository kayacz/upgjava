

import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;

public class Chart1 {
	public NumberAxis xAxis;//x-ov� osa
	public NumberAxis yAxis;//y-osa
	public double elevation;
	public double v0;

	public Chart1() {
		super();
	}

	public NumberAxis getxAxis() {
		return xAxis;
	}

	public NumberAxis getyAxis() {
		return yAxis;
	}

	public double getElevation() {
		return elevation;
	}

	public double getV0() {
		return v0;
	}

	public void setxAxis(NumberAxis xAxis) {
		this.xAxis = xAxis;
	}

	public void setyAxis(NumberAxis yAxis) {
		this.yAxis = yAxis;
	}

	public void setElevation(double elevation) {
		this.elevation = elevation;
	}

	public void setV0(double v0) {
		this.v0 = v0;
	}
	/**
	 * vytvo�en� a napln�n� grafu daty
	 */
	public LineChart<Number, Number> drawChart() {
		xAxis = new NumberAxis();
		yAxis = new NumberAxis();

		xAxis.setLabel("po��te�n� rychlost/m*s");
		yAxis.setLabel("dost�el/m");
		LineChart<Number, Number> lineChart = new LineChart<Number, Number>(xAxis, yAxis);

		lineChart.setTitle("z�vislost dost�elu na po��te�n� rychlosti st�ely");
		XYChart.Series series = new XYChart.Series();
		series.setName("vdalenost dopadu strely");
		lineChart.setCreateSymbols(false);
		for (int i = 0; i < 2*this.getV0(); i++) {
			double temp = (((i * i)) / 10) * Math.sin(2*Math.toRadians(this.getElevation()));
			series.getData().add(new XYChart.Data<Integer, Double>(i, temp));
		}
		lineChart.getData().add(series);
		return lineChart;

	}
}
